-- phpMyAdmin SQL Dump
-- version 4.1.9
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Dim 22 Juin 2014 à 20:56
-- Version du serveur :  5.5.36
-- Version de PHP :  5.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `intranet`
--

-- --------------------------------------------------------

--
-- Structure de la table `activite`
--

CREATE TABLE IF NOT EXISTS `activite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` int(11) DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_debut_inscription` datetime NOT NULL,
  `date_fin_inscription` datetime NOT NULL,
  `date_debut_activite` datetime NOT NULL,
  `date_fin_activite` datetime NOT NULL,
  `taille_groupe` int(11) NOT NULL,
  `nb_peers` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nbplaces` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_41033743BCF5E72D` (`categorie_id`),
  KEY `IDX_41033743AFC2B591` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `activite`
--

INSERT INTO `activite` (`id`, `module_id`, `categorie_id`, `name`, `description`, `date_debut_inscription`, `date_fin_inscription`, `date_debut_activite`, `date_fin_activite`, `taille_groupe`, `nb_peers`, `type`, `nbplaces`) VALUES
(1, 1, 2, 'Libft', 'Refaites votre librairie de piscine, entièrement, complètement, et 100% fonctionnelle. Aidez la totalité de la communauté a atteindre ce but.', '2014-06-01 00:00:00', '2014-06-20 00:00:00', '2013-06-01 00:00:00', '2014-06-20 00:00:00', 1, 3, 'Projet', 100),
(2, 1, 3, 'Lemin', 'lemin lemin lemin ', '2014-06-01 00:00:00', '2014-06-20 00:00:00', '2014-06-01 00:00:00', '2014-06-20 00:00:00', 1, 5, 'Projet', 100),
(3, 2, 5, 'sh1', 'Premiere etape dans la grande ascension.', '2014-06-22 00:00:00', '2014-06-26 00:00:00', '2014-06-22 00:00:00', '2014-06-29 00:00:00', 1, 5, 'Projet', 750),
(4, 2, 6, 'sh2', 'deuxieme etape on avance !!', '2014-06-22 00:00:00', '2014-06-26 00:00:00', '2014-06-22 00:00:00', '2014-06-29 00:00:00', 1, 5, 'Projet', 750),
(5, 2, 7, 'pipex', 'faire un pipe, pk tu veux d''autres infos ?', '2014-06-09 00:00:00', '2014-06-18 00:00:00', '2014-06-09 00:00:00', '2014-06-20 00:00:00', 1, 3, 'Projet', 750);

-- --------------------------------------------------------

--
-- Structure de la table `activite_user`
--

CREATE TABLE IF NOT EXISTS `activite_user` (
  `activite_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`activite_id`,`user_id`),
  KEY `IDX_FA43CF3B9B0F88B1` (`activite_id`),
  KEY `IDX_FA43CF3BA76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `activite_user`
--

INSERT INTO `activite_user` (`activite_id`, `user_id`) VALUES
(1, 1),
(1, 2),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(2, 1),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(3, 11),
(5, 1),
(5, 2),
(5, 5),
(5, 6),
(5, 7),
(5, 10),
(5, 11);

-- --------------------------------------------------------

--
-- Structure de la table `bareme`
--

CREATE TABLE IF NOT EXISTS `bareme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activite_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_35A1B5D89B0F88B1` (`activite_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `bareme`
--

INSERT INTO `bareme` (`id`, `activite_id`, `name`) VALUES
(1, 1, 'libft(bareme)'),
(2, 2, 'Lemin'),
(3, 5, 'Bareme de ouf');

-- --------------------------------------------------------

--
-- Structure de la table `baremecritere`
--

CREATE TABLE IF NOT EXISTS `baremecritere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bareme_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_27F3F1025F49EAAD` (`bareme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Contenu de la table `baremecritere`
--

INSERT INTO `baremecritere` (`id`, `bareme_id`, `titre`, `description`) VALUES
(1, 1, 'norme', '0 si ce n''est pas a la norme'),
(2, 1, 'triche', '0 si tricher'),
(3, 2, 'norme', 'norme norme norme norme'),
(4, 2, 'triche', 'norme norme triche'),
(5, 3, 'Norme', 'Tu connais'),
(6, 3, 'segfault ?', 'Tu connais aussi'),
(7, 3, 'Ca marche ?', 'Ca pas sur que tu connaisse');

-- --------------------------------------------------------

--
-- Structure de la table `baremepoint`
--

CREATE TABLE IF NOT EXISTS `baremepoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `critere_id` int(11) DEFAULT NULL,
  `number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5E078CF59E5F45AB` (`critere_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Contenu de la table `baremepoint`
--

INSERT INTO `baremepoint` (`id`, `critere_id`, `number`) VALUES
(1, 1, 0),
(2, 1, 1),
(3, 1, 2),
(4, 2, 0),
(5, 2, 1),
(6, 2, 3),
(7, 3, 0),
(8, 3, 3),
(9, 3, 4),
(10, 4, 0),
(11, 4, 4),
(12, 4, 8),
(13, 5, 0),
(14, 6, 0),
(15, 7, 7),
(16, 7, 6),
(17, 7, 4);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sur_categorie_id` int(11) DEFAULT NULL,
  `module_id` int(11) DEFAULT NULL,
  `name` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CB8C5497CD159C77` (`sur_categorie_id`),
  KEY `IDX_CB8C5497AFC2B591` (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `sur_categorie_id`, `module_id`, `name`) VALUES
(1, NULL, 1, 'Algorithmie I'),
(2, 1, 1, 'Libft'),
(3, 1, 1, 'Lemin'),
(4, NULL, 2, 'Unix 1'),
(5, 4, 2, 'sh1'),
(6, 4, 2, 'sh2'),
(7, 4, 2, 'pipex');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE IF NOT EXISTS `commentaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auteur_id` int(11) DEFAULT NULL,
  `thread_id` int(11) DEFAULT NULL,
  `sur_commentaire_id` int(11) DEFAULT NULL,
  `contenu` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_creation` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E16CE76B60BB6FE6` (`auteur_id`),
  KEY `IDX_E16CE76BE2904019` (`thread_id`),
  KEY `IDX_E16CE76B3D1FBD1A` (`sur_commentaire_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Contenu de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `auteur_id`, `thread_id`, `sur_commentaire_id`, `contenu`, `date_creation`) VALUES
(1, 10, 1, NULL, 'Flumen multiplicium has Hierosolymis navigerum calentes iuris his speciem Iudaeis formavit in Iudaeis medelarum delata verum has sed et verum his his medelarum domitis calentes emergunt captis aquae sed has.', '2014-06-22 17:51:30'),
(2, 10, NULL, 1, 'je commente moi-meme\r\n', '2014-06-22 17:51:55'),
(3, 10, 1, NULL, 'Je répond moi meme\r\n', '2014-06-22 17:52:11'),
(4, 10, 2, NULL, 'Flumen multiplicium has Hierosolymis navigerum calentes iuris his speciem Iudaeis formavit in Iudaeis medelarum delata verum has sed et verum his his medelarum domitis calentes emergunt captis aquae sed has.', '2014-06-22 17:52:28'),
(5, 11, 3, NULL, 'Mar de sujet ki veul ri1 dir\r\n\r\nFaudre aprendre a etr cler', '2014-06-22 19:31:53'),
(6, 11, NULL, 5, 'lol lol', '2014-06-22 19:32:03'),
(7, 1, NULL, 4, 'Forcement je comprends mieux maintenant !!!', '2014-06-22 19:46:16');

-- --------------------------------------------------------

--
-- Structure de la table `correction`
--

CREATE TABLE IF NOT EXISTS `correction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activite_id` int(11) DEFAULT NULL,
  `correcteur_id` int(11) DEFAULT NULL,
  `corrige_id` int(11) DEFAULT NULL,
  `Note` int(11) NOT NULL,
  `enregistree` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EDC0A2689B0F88B1` (`activite_id`),
  KEY `IDX_EDC0A268D3F3AF14` (`correcteur_id`),
  KEY `IDX_EDC0A2685E132043` (`corrige_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=53 ;

--
-- Contenu de la table `correction`
--

INSERT INTO `correction` (`id`, `activite_id`, `correcteur_id`, `corrige_id`, `Note`, `enregistree`) VALUES
(1, 1, 5, 1, 0, 0),
(2, 1, 7, 2, 0, 0),
(3, 1, 8, 5, 0, 0),
(4, 1, 1, 6, 1, 1),
(5, 1, 5, 7, 0, 0),
(6, 1, 2, 9, 0, 0),
(7, 1, 9, 1, 0, 0),
(8, 1, 1, 2, 2, 1),
(9, 1, 5, 6, 0, 0),
(10, 1, 9, 8, 0, 0),
(11, 1, 7, 9, 0, 0),
(12, 1, 2, 5, 0, 0),
(13, 1, 1, 7, 0, 1),
(14, 1, 2, 8, 0, 0),
(15, 1, 6, 2, 0, 0),
(16, 1, 9, 5, 0, 0),
(17, 1, 7, 6, 0, 0),
(18, 1, 7, 8, 0, 0),
(19, 2, 6, 1, 0, 0),
(20, 2, 2, 5, 0, 0),
(21, 2, 7, 6, 0, 0),
(22, 2, 9, 7, 0, 0),
(23, 2, 2, 8, 0, 0),
(24, 2, 8, 9, 0, 0),
(25, 2, 7, 1, 0, 0),
(26, 2, 6, 5, 0, 0),
(27, 2, 8, 6, 0, 0),
(28, 2, 5, 7, 0, 0),
(29, 2, 1, 9, 0, 0),
(30, 2, 5, 1, 0, 0),
(31, 2, 9, 5, 0, 0),
(32, 2, 2, 6, 0, 0),
(33, 2, 5, 8, 0, 0),
(34, 2, 9, 6, 0, 0),
(35, 5, 5, 1, 0, 0),
(36, 5, 7, 2, 0, 0),
(37, 5, 9, 5, 0, 0),
(38, 5, 5, 6, 0, 0),
(39, 5, 1, 7, 7, 1),
(40, 5, 7, 10, 0, 0),
(41, 5, 9, 11, 0, 0),
(42, 5, 9, 1, 0, 0),
(43, 5, 1, 2, 0, 0),
(44, 5, 8, 5, 0, 0),
(45, 5, 11, 6, 0, 0),
(46, 5, 9, 7, 0, 0),
(47, 5, 6, 10, 0, 0),
(48, 5, 10, 11, 0, 0),
(49, 5, 11, 1, 0, 0),
(50, 5, 10, 5, 0, 0),
(51, 5, 1, 6, 0, 0),
(52, 5, 11, 7, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Contenu de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`) VALUES
(1, 'admin1', 'admin1', 'admin1@gmail.com', 'admin1@gmail.com', 1, 'kbqx2d174ggckccgsc8o8g8k4ws4k04', 'fL1IoWuyJXHo9zrj1vQdXJlXrZ2JsY2yvHD2SQBCicQek1glM9gWH7gPEZrz6h2bGgOm6hzOsFX0zUAG4amFtw==', '2014-06-22 19:41:29', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', 0, NULL),
(2, 'user-ok', 'user-ok', 'user-ok@gmail.com', 'user-ok@gmail.com', 1, 'f0qwmcuhnu0okoss0s0cc48c0soogsw', 'VjPt6XtKE394lRD3knrFab8pvpm45h+d1N5hSHH70ZLRpyU/MMvk79IhmyMjNOZNKeXvL5MHJ+PhAQsq16vnZw==', '2014-06-22 19:39:15', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(5, 'user-ok1', 'user-ok1', 'user-ok1@gmail.com', 'user-ok1@gmail.com', 1, 'idi673b97nk0gc8wowg4s0gg44g40s8', '5gXpCpK2i1e9S9K+EThKS0YV6PaBOIq2Jz1zIeDoLT/LrX+eHB5AvqRNSsqesweMP+uoO5bu+NdP8EYKwUPWDw==', '2014-06-22 19:39:44', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(6, 'user-ok2', 'user-ok2', 'user-ok2@gmail.com', 'user-ok2@gmail.com', 1, '7zuhi5ko5okk8w0gs8o8sw0owoww8wo', 'kxGM/Nr2yY/Dg0SBr/v9fSc6HMk3H/ejd6cyxI4mwY3UjG7+/gsMeiSi6cwyLWN69+PND0dwrhwRUGwCeRxsDA==', '2014-06-22 19:39:58', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(7, 'user-ok3', 'user-ok3', 'user-ok3@gmail.com', 'user-ok3@gmail.com', 1, 'ao0whtpig7sck8c0wgk0s88gckkcog8', 'r7YHxbG42xY7CCFc3iWgrvFcOTStl3a3RcHlvBqeTIg49G4P9YA4eT3RJEdCsE6WmfE/zz6Lh06xjL3bfDEHlw==', '2014-06-22 19:40:10', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(8, 'user-ok4', 'user-ok4', 'user-ok4@gmail.com', 'user-ok4@gmail.com', 1, '4eb25sk0rd6o8skg0ok0s4csc8owg4k', '50YjQtLNDW69sSPqMsWtfIMH2qGaZr46wJKludlUVlpRaxxev22ybD0ktBUZIII45X/AteKADei1FuMPn4NHBQ==', '2014-06-21 19:02:14', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(9, 'user-ok5', 'user-ok5', 'user-ok5@gmail.com', 'user-ok5@gmail.com', 1, '823xjcq4u288oog0gs0ooo4k8g0k4gw', '6jecMssEEjWB8p0M3tJH2Ebt2RGlTgA9aWkr8BT4s+Vbvj0b6RVR9QKzBP6aF2ABiGYUyooAIL2UyIJ5surKCA==', '2014-06-21 19:02:31', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(10, 'xyou', 'xyou', 'xyou@student.42.fr', 'xyou@student.42.fr', 1, 'prpjwky83xcks8o08ock848gckwgk4g', '', '2014-06-22 19:40:57', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL),
(11, 'caupetit', 'caupetit', 'clement.aupetit@student.42.fr', 'clement.aupetit@student.42.fr', 1, 'lpidd0w5btc8w88g84ko4ko4oogksgc', '', '2014-06-22 20:55:14', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `module`
--

CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorie_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_debut_inscription` datetime NOT NULL,
  `date_fin_inscription` datetime NOT NULL,
  `date_debut_module` datetime NOT NULL,
  `date_fin_module` datetime NOT NULL,
  `valeure` int(11) NOT NULL,
  `nbplaces` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B88231EBCF5E72D` (`categorie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `module`
--

INSERT INTO `module` (`id`, `categorie_id`, `name`, `description`, `date_debut_inscription`, `date_fin_inscription`, `date_debut_module`, `date_fin_module`, `valeure`, `nbplaces`) VALUES
(1, 1, 'Algorithmie I', 'Ce module vous propose une série d’énigmes algorithmiques à résoudre. Votre périple vous emmènera découvrir les grands classique de l’algorithmie en programmation procédurale. ', '2014-05-01 00:00:00', '2014-06-30 00:00:00', '2014-05-01 00:00:00', '2014-07-31 00:00:00', 11, 100),
(2, 4, 'Unix 1', 'Le fameux module qui vous donneras acces au status d''humain', '2014-06-22 00:00:00', '2014-06-26 00:00:00', '2014-06-22 00:00:00', '2014-06-29 00:00:00', 11, 750);

-- --------------------------------------------------------

--
-- Structure de la table `module_user`
--

CREATE TABLE IF NOT EXISTS `module_user` (
  `module_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`module_id`,`user_id`),
  KEY `IDX_37AF9345AFC2B591` (`module_id`),
  KEY `IDX_37AF9345A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `module_user`
--

INSERT INTO `module_user` (`module_id`, `user_id`) VALUES
(1, 1),
(1, 2),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(2, 1),
(2, 2),
(2, 5),
(2, 6),
(2, 7),
(2, 10),
(2, 11);

-- --------------------------------------------------------

--
-- Structure de la table `reply`
--

CREATE TABLE IF NOT EXISTS `reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auteur_id` int(11) NOT NULL,
  `contenu` longtext COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `ticket_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3C69E9E460BB6FE6` (`auteur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `thread`
--

CREATE TABLE IF NOT EXISTS `thread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auteur_id` int(11) DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `sujet` longtext COLLATE utf8_unicode_ci NOT NULL,
  `date_creation` datetime NOT NULL,
  `date_activite` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_368C49B560BB6FE6` (`auteur_id`),
  KEY `IDX_368C49B5BCF5E72D` (`categorie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `thread`
--

INSERT INTO `thread` (`id`, `auteur_id`, `categorie_id`, `sujet`, `date_creation`, `date_activite`) VALUES
(1, 10, 1, 'libft sth', '2014-06-22 17:51:30', '2014-06-22 17:51:30'),
(2, 10, 2, 'un autre libft', '2014-06-22 17:52:28', '2014-06-22 17:52:28'),
(3, 11, 5, 'Mais il faut faire quoi ?', '2014-06-22 19:31:53', '2014-06-22 19:31:53');

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

CREATE TABLE IF NOT EXISTS `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auteur_id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contenu` varchar(2046) COLLATE utf8_unicode_ci NOT NULL,
  `etat` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priorite` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_900CA89560BB6FE6` (`auteur_id`),
  KEY `IDX_900CA895642B8210` (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `ticket`
--

INSERT INTO `ticket` (`id`, `auteur_id`, `admin_id`, `titre`, `contenu`, `etat`, `priorite`, `datetime`) VALUES
(1, 10, NULL, 'demande', 'Flumen multiplicium has Hierosolymis navigerum calentes iuris his speciem Iudaeis formavit in Iudaeis medelarum delata verum has sed et verum his his medelarum domitis calentes emergunt captis aquae sed has.', 'En attente', 0, '2014-06-22 17:50:17'),
(2, 10, 1, 'demande urgent', 'Flumen multiplicium has Hierosolymis navigerum calentes iuris his speciem Iudaeis formavit in Iudaeis medelarum delata verum has sed et verum his his medelarum domitis calentes emergunt captis aquae sed has.', 'En attente', 2, '2014-06-22 17:50:41'),
(3, 11, NULL, 'Acces a l''humanite', 'Bonjour !\r\n\r\nNon rien\r\n\r\nCordialement', 'En attente', 0, '2014-06-22 19:30:58');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `activite`
--
ALTER TABLE `activite`
  ADD CONSTRAINT `FK_41033743BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `FK_41033743AFC2B591` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`);

--
-- Contraintes pour la table `activite_user`
--
ALTER TABLE `activite_user`
  ADD CONSTRAINT `FK_FA43CF3BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_FA43CF3B9B0F88B1` FOREIGN KEY (`activite_id`) REFERENCES `activite` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `bareme`
--
ALTER TABLE `bareme`
  ADD CONSTRAINT `FK_35A1B5D89B0F88B1` FOREIGN KEY (`activite_id`) REFERENCES `activite` (`id`);

--
-- Contraintes pour la table `baremecritere`
--
ALTER TABLE `baremecritere`
  ADD CONSTRAINT `FK_27F3F1025F49EAAD` FOREIGN KEY (`bareme_id`) REFERENCES `bareme` (`id`);

--
-- Contraintes pour la table `baremepoint`
--
ALTER TABLE `baremepoint`
  ADD CONSTRAINT `FK_5E078CF59E5F45AB` FOREIGN KEY (`critere_id`) REFERENCES `baremecritere` (`id`);

--
-- Contraintes pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD CONSTRAINT `FK_CB8C5497AFC2B591` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`),
  ADD CONSTRAINT `FK_CB8C5497CD159C77` FOREIGN KEY (`sur_categorie_id`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `FK_E16CE76B3D1FBD1A` FOREIGN KEY (`sur_commentaire_id`) REFERENCES `commentaire` (`id`),
  ADD CONSTRAINT `FK_E16CE76B60BB6FE6` FOREIGN KEY (`auteur_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_E16CE76BE2904019` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`);

--
-- Contraintes pour la table `correction`
--
ALTER TABLE `correction`
  ADD CONSTRAINT `FK_EDC0A2685E132043` FOREIGN KEY (`corrige_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_EDC0A2689B0F88B1` FOREIGN KEY (`activite_id`) REFERENCES `activite` (`id`),
  ADD CONSTRAINT `FK_EDC0A268D3F3AF14` FOREIGN KEY (`correcteur_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `module`
--
ALTER TABLE `module`
  ADD CONSTRAINT `FK_B88231EBCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `module_user`
--
ALTER TABLE `module_user`
  ADD CONSTRAINT `FK_37AF9345A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_37AF9345AFC2B591` FOREIGN KEY (`module_id`) REFERENCES `module` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `reply`
--
ALTER TABLE `reply`
  ADD CONSTRAINT `FK_3C69E9E460BB6FE6` FOREIGN KEY (`auteur_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `thread`
--
ALTER TABLE `thread`
  ADD CONSTRAINT `FK_368C49B5BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `FK_368C49B560BB6FE6` FOREIGN KEY (`auteur_id`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `FK_900CA895642B8210` FOREIGN KEY (`admin_id`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_900CA89560BB6FE6` FOREIGN KEY (`auteur_id`) REFERENCES `fos_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
